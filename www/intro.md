---
title: "Intro"
output: html_document
---

# Welcome to this Shiny ROCK app!

This Shiny App was developed for cognitive interviews. It allows you to paste a ROCK source and will then parse it and produce a Cognitive Interview heatmap. This heatmap shows which codes occur for which items.

The app assumes you followed the ROCK conventions, using double square brackets to denote code identifiers; and that you use the `ci--` prefix to denote cognitive interviewing codes and the `uiid:` prefix to denote which items data relates to (`uiid` stands for Unique Item Identifier).

This is an example of a valid source. To try out the app, you can copy-paste this text into the text area in the second tab.

```
###-----------------------------------------------------------------------------

How large is your family? [[uiid:familySize_7djdy62d]]

Participant also counts 3 dogs and 7 goldfish [[ci--understanding]]

Participant does not count own brothers and sisters, only their partner and children [[ci--retrieval]]

###-----------------------------------------------------------------------------

How many windows are there in your house? [[uiid:windows_7djdy62d]]

Participant does not live in a house, but in an apartment [[ci--content_adequacy]]

Participant also counts windows in doors inside the house [[ci--understanding]]

###-----------------------------------------------------------------------------
```
